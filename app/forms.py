from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, SelectField
from wtforms.validators import DataRequired, NumberRange

class NumberForm(FlaskForm):
    number = IntegerField('number', validators=[NumberRange(min=1, message="Invalid number!"), DataRequired()])
    numberType = SelectField('type', choices=[('range', 'normal range'), ('odd', 'odd'), ('even', 'even'), ('prime', 'prime')], validators=[DataRequired()])
    submit = SubmitField('go')