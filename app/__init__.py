from flask import Flask, render_template, request, flash, redirect, url_for
from flask_paginate import Pagination, get_page_parameter
from app.forms import NumberForm
import os, math

per_page = 500

# factory function for application
def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    @app.route('/', methods=["GET", "POST"])
    def index():
        form = NumberForm()
        if form.validate_on_submit():
            numberType = form.numberType.data
            num = form.number.data
            if numberType == "odd":
                return redirect(url_for("odd_number", number=num))
            elif numberType == "even":
                return redirect(url_for("even_number", number=num))
            elif numberType == "prime":
                return redirect(url_for("prime_number", number=num))
            else:
                return redirect(url_for("number", number=num)) 
        return render_template("home.html", form=form)

    @app.route('/<int:number>')
    def number(number):
        # pagination
        page = request.args.get(get_page_parameter(), type=int, default=1)
        pagination = Pagination(page=page, total=number, per_page=per_page)
        start = per_page * (page-1) + 1
        if start + per_page > number:
            end = number + 1
        else:
            end = start + per_page
        numbers = [num for num in range(start, end)]
        return render_template('numbers.html', numbers=numbers, number=number, pagination=pagination)

    @app.route('/<int:number>/even')
    def even_number(number):  
        # pagination       
        page = request.args.get(get_page_parameter(), type=int, default=1)
        pagination = Pagination(page=page, total=number//2, per_page=per_page)
        start = (per_page * 2) * (page-1) + 1
        if start + (per_page * 2) > number:
            end = number + 1
        else:
            end = start + (per_page * 2)
        numbers = [num for num in range(start, end) if num % 2 == 0]
        return render_template('numbers.html', numbers=numbers, type="even", number=number, pagination=pagination)
    
    @app.route('/<int:number>/odd')
    def odd_number(number):
        #pagination
        page = request.args.get(get_page_parameter(), type=int, default=1)
        pagination = Pagination(page=page, total=math.ceil(number/2), per_page=per_page)
        start = (per_page * 2) * (page-1) + 1
        if start + (per_page * 2) > number:
            end = number + 1
        else:
            end = start + (per_page * 2)
        numbers = [num for num in range(start, end) if num % 2 == 1]
        return render_template('numbers.html', numbers=numbers, type="odd", number=number, pagination=pagination)
    
    @app.route('/<int:number>/prime')
    def prime_number(number):
        # sieve of eratothenes for prime #'s
        primes = [True for i in range(0, number+1)]
        p = 2
        while (p*p <= number):
            if primes[p]:
                for i in range(p*p, number+1, p):
                    primes[i] = False
            p += 1
        
        numbers = [num for num in range(2, number+1) if primes[num]]

        #pagination
        page = request.args.get(get_page_parameter(), type=int, default=1)
        pagination = Pagination(page=page, total=len(numbers), per_page=per_page)

        # array zero indexed for slicing
        start = per_page * (page-1) 
        if start + per_page  > len(numbers):
            end = number + 1
        else:
            end = start + per_page 
        render = numbers[start:end]
        return render_template('numbers.html', numbers=render, type="prime", number=number, pagination=pagination)

    return app
