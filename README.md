# Samantha Fu - SAP Numbers App

- Root / will display a form allowing user to choose number and type of range
- /\<int:number\> will display integers from 1 to that number
- /\<int:number\>/odd will display only odd numbers in that range
- /\<int:number\>/even will display only even numbers in that range
- /\<int:number\>/prime will display only prime numbers in that range

## Test

For local installation and testing, clone this repository and create a python virtual
environment with python 3.7.7 Activate the environment and install flask, flask-paginate,
and flask-wtf (or install using requirements.txt).

Then set the FLASK_APP environment variable to be app, and run `flask run` to host locally.